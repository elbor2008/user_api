using FluentAssertions;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Threading.Tasks;
using User.API.Controllers;
using User.API.Data;
using User.API.Models;
using Xunit;

namespace User.API.UnitTests
{
    public class UsersControllerUnitTests
    {
        private readonly static UserContext _userContext;
        static UsersControllerUnitTests()
        {
            DbContextOptionsBuilder<UserContext> optionBuilder = new DbContextOptionsBuilder<UserContext>();
            DbContextOptions<UserContext> options = optionBuilder.UseInMemoryDatabase("db").Options;
            _userContext = new UserContext(options);
            _userContext.Users.Add(new AppUser()
            {
                Id = 1,
                Name = "Nick"
            });
            _userContext.SaveChanges();
        }

        [Fact]
        public async Task Get_ReturnUsers_WithNoParameter()
        {
            UsersController usersController = new UsersController(_userContext);
            IActionResult response = await usersController.Get();
            OkObjectResult result = response.Should().BeOfType<OkObjectResult>().Subject;
            AppUser user = result.Value.Should().BeAssignableTo<AppUser>().Subject;
            user.Id.Should().Be(1);
            user.Name.Should().Be("Nick");
        }
        [Fact]
        public async Task Patch_ReturnUser_WithExpectedParameter()
        {
            UsersController usersController = new UsersController(_userContext);
            JsonPatchDocument patch = new JsonPatchDocument();
            patch.Replace("/phone", "112233");
            IActionResult response = await usersController.Patch(patch);
            OkObjectResult result = response.Should().BeOfType<OkObjectResult>().Subject;
            AppUser user = result.Value.Should().BeAssignableTo<AppUser>().Subject;
            user.Phone.Should().Be("112233");
        }
    }
}
