﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using User.API.Data;
using User.API.Models;

namespace User.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : BaseController
    {
        private readonly UserContext _userContext;

        public UsersController(UserContext userContext)
        {
            _userContext = userContext;
        }

        [Route("")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            AppUser user = await _userContext.Users.AsNoTracking().Include(u => u.UserProperties).SingleOrDefaultAsync(u => u.Id == UserIdentity.UserId);
            if (null == user)
            {
                throw new UserOperationException($"Wrong user context id: {UserIdentity.UserId}");
            }
            return Ok(user);
        }
        [Route("")]
        [HttpPatch]
        public async Task<IActionResult> Patch(JsonPatchDocument patch)
        {
            AppUser user = await _userContext.Users.Include(u => u.UserProperties).SingleOrDefaultAsync(u => u.Id == UserIdentity.UserId);
            if (null == user)
            {
                throw new UserOperationException($"Wrong user context id: {UserIdentity.UserId}");
            }
            patch.ApplyTo(user);
            _userContext.Users.Update(user);
            await _userContext.SaveChangesAsync();
            return Ok(user);
        }
    }
}