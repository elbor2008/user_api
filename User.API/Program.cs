﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using User.API.Data;
using User.API.Models;

namespace User.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IWebHost webHost = CreateWebHostBuilder(args).Build();
            using (IServiceScope scope = webHost.Services.CreateScope())
            {
                IServiceProvider serviceProvider = scope.ServiceProvider;
                IHostingEnvironment hostingEnvironment = serviceProvider.GetRequiredService<IHostingEnvironment>();
                if (hostingEnvironment.IsProduction())
                {
                    Thread.Sleep(5000);
                }
                UserContext userContext = serviceProvider.GetRequiredService<UserContext>();
                if (userContext.Database.GetPendingMigrations().Any())
                {
                    userContext.Database.Migrate();
                }
                if (userContext.Users.Count() == 0)
                {
                    userContext.Users.Add(new AppUser()
                    {
                        Name = "Nick",
                        Company = "Google"
                    });
                    userContext.SaveChanges();
                }
            }
            webHost.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>()
            .UseUrls("http://+:80");
    }
}
