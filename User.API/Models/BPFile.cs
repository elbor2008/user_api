﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace User.API.Models
{
    public class BPFile
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string FileName { get; set; }
        /// <summary>
        /// Origin file path to upload
        /// </summary>
        public string OriginFilePath { get; set; }
        /// <summary>
        /// File path after format
        /// </summary>
        public string FormatFilePath { get; set; }
        public DateTime CreatedTime { get; set; }
    }
}
